.. index::
   pair: Définition antisémitisme ; AIMH


.. _def_antisemitisme_aimh:

================================================================================
Définition AIMH (Alliance internationale pour la mémoire de l’Holocauste)
================================================================================

.. seealso::

   - https://www.holocaustremembrance.com/fr/resources/working-definitions-charters/la-definition-operationnelle-de-lantisemitisme-utilisee-par

.. contents::
   :depth: 3


Introduction
===============

L’Alliance internationale pour la mémoire de l’Holocauste (IHRA) rassemble
des gouvernements et des experts dans le but de renforcer et de promouvoir
l’éducation, le travail de mémoire et la recherche sur l’Holocauste et
de mettre en œuvre les engagements de la déclaration de Stockholm de 2000.

La définition opérationnelle de l’antisémitisme, non contraignante, a
été adoptée par les 31 États membres de l’IHRA le 26 mai 2016:

«L’antisémitisme est une certaine perception des Juifs qui peut se manifester
par une haine à leur égard. Les manifestations rhétoriques et physiques
de l’antisémitisme visent des individus juifs ou non et/ou leurs biens,
des institutions communautaires et des lieux de culte.»

Les exemples suivants, destinés à guider le travail de l’IHRA, illustrent cette définition:

L’antisémitisme peut se manifester par des attaques à l’encontre de
l’État d’Israël lorsqu’il est perçu comme une collectivité juive.

Cependant, critiquer Israël comme on critiquerait tout autre État ne
peut pas être considéré comme de l’antisémitisme.
L’antisémitisme consiste souvent à accuser les Juifs de conspirer
contre l’humanité et, ce faisant, à les tenir responsables de
«tous les problèmes du monde».
Il s’exprime à l’oral, à l’écrit, de façon graphique ou par des actions,
et fait appel à des stéréotypes inquiétants et à des traits de caractère
péjoratifs.

Parmi les exemples contemporains d’antisémitisme dans la vie publique,
les médias, les écoles, le lieu de travail et la sphère religieuse,
on peut citer, en fonction du contexte et de façon non exhaustive:

- l’appel au meurtre ou à l’agression de Juifs, la participation à ces
  agissements ou leur justification au nom d’une idéologie radicale ou
  d’une vision extrémiste de la religion;
- la production d’affirmations fallacieuses, déshumanisantes, diabolisantes
  ou stéréotypées sur les Juifs ou le pouvoir des Juifs en tant que collectif
  comme notamment, mais pas uniquement, le mythe d’un complot juif ou
  d’un contrôle des médias, de l’économie, des pouvoirs publics ou
  d’autres institutions par les Juifs;
- le reproche fait au peuple juif dans son ensemble d’être responsable
  d’actes, réels ou imaginaires, commis par un seul individu ou groupe
  juif, ou même d’actes commis par des personnes non juives;
- la négation des faits, de l’ampleur, des procédés (comme les chambres à gaz)
  ou du caractère intentionnel du génocide du peuple juif perpétré par
  l’Allemagne nationale-socialiste et ses soutiens et complices pendant
  la Seconde Guerre mondiale (l’Holocauste);
- le reproche fait au peuple juif ou à l’État d’Israël d’avoir inventé
  ou d’exagérer l’Holocauste;
- le reproche fait aux citoyens juifs de servir davantage Israël ou les
  priorités supposés des Juifs à l’échelle mondiale que les intérêts de
  leur propre pays;
- le refus du droit à l’autodétermination des Juifs, en affirmant par
  exemple que l’existence de l’État d’Israël est le fruit d’une entreprise raciste;
- le traitement inégalitaire de l’État d’Israël, à qui l’on demande
  d’adopter des comportements qui ne sont ni attendus ni exigés de tout
  autre État démocratique;
- l’utilisation de symboles et d’images associés à l’antisémitisme
  traditionnel (comme l’affirmation selon laquelle les Juifs auraient
  tué Jésus ou pratiqueraient des sacrifices humains) pour caractériser
  Israël et les Israéliens;
- l’établissement de comparaisons entre la politique israélienne
  contemporaine et celle des Nazis;
- l’idée selon laquelle les Juifs seraient collectivement responsables
  des actions de l’État d’Israël.


Un acte antisémite est une infraction lorsqu’il est qualifié ainsi par
la loi (c’est le cas, par exemple, du déni de l’existence de l’Holocauste
ou de la diffusion de contenus antisémites dans certains pays).

Une infraction est qualifiée d’antisémite lorsque les victimes ou les
biens touchés (comme des bâtiments, des écoles, des lieux de culte et
des cimetières) sont ciblés parce qu’ils sont juifs ou relatifs aux Juifs,
ou perçus comme tels.

La discrimination à caractère antisémite est le fait de refuser à des
Juifs des possibilités ou des services ouverts à d’autres.

Elle est illégale dans de nombreux pays.

