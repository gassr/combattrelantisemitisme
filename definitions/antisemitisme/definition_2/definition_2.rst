.. index::
   pair: Définition antisémitisme ; médiapart (2020-11-30)

.. _def_antisemitisme_2:

=================================
Définition 2
=================================

.. seealso::

   - https://blogs.mediapart.fr/les-invites-de-mediapart/blog/301120/declaration-sur-l-antisemitisme-et-la-question-de-palestine

.. contents::
   :depth: 3


Introduction
===============

Cent vingt-deux intellectuels et intellectuelles palestiniens et arabes
répondent à la définition de l’antisémitisme promue par l’Alliance
internationale pour la mémoire de l’Holocauste qui est utilisée, dans
plusieurs pays d’Europe et d’Amérique, pour réprimer le soutien aux droits
palestiniens.

Cette Déclaration est publiée conjointement en arabe par Al-Quds (Londres)
et Al-Ayyam (Ramallah), en hébreu par Haaretz (Tel-Aviv), en anglais par
The Guardian (Londres), en allemand par Die Tageszeitung (Berlin) et en
français par Mediapart.

Plus de 120 intellectuels et intellectuelles palestiniens et arabes
répondent dans une déclaration publique à l’adoption croissante de la
définition de l’antisémitisme produite par l’Alliance internationale
pour la mémoire de l’Holocauste (AIMH) et à la manière dont cette
définition est utilisée afin de réprimer le soutien aux droits
palestiniens dans plusieurs pays d’Europe et d’Amérique du Nord.

Les signataires maintiennent que le combat contre l’antisémitisme est
instrumentalisé par le gouvernement israélien et ses partisans dans le
but de délégitimer et de réduire au silence la défense des droits palestiniens.

Les signataires de la lettre ouverte reconnaissent que l’antisémitisme
est un problème réel et croissant en Europe et en Amérique du Nord,
conjointement avec la croissance générale de tous types de racismes et
de mouvements d’extrême droite.

Ils sont entièrement déterminés à le combattre et le discréditer, tout
en étant persuadés que la lutte contre l’antisémitisme bien compris est
parfaitement compatible avec la lutte pour que justice soit faite pour
les Palestiniens en tant que lutte anticoloniale.

L’utilisation de l’antisémitisme afin de tenter de délégitimer la cause
palestinienne pervertit et détourne de son objectif le combat contre la
persistance et la résurgence de l’antisémitisme.

Les signataires de la Déclaration conçoivent la lutte contre l’antisémitisme
comme étant autant une lutte pour l’émancipation humaine et politique
que ne l’est la résistance palestinienne à l’occupation et à la spoliation étatique.

Cette Déclaration est publiée conjointement, lundi 30 novembre 2020, en
arabe par Al-Quds (Londres) et Al-Ayyam (Ramallah), en hébreu par Haaretz (Tel-Aviv),
en anglais par The Guardian (Londres), en allemand par Die Tageszeitung (Berlin)
et en français par Mediapart.

Déclaration sur l’antisémitisme et la question de Palestine
================================================================

Nous, soussignés, universitaires, artistes, journalistes, et intellectuels,
Palestiniens et Arabes, affirmons par la présente déclaration notre
position au sujet de la définition de l’antisémitisme produite par
l’Alliance internationale pour la mémoire de l’Holocauste (AIMH), ainsi
que sur son adoption, interprétation et diffusion dans plusieurs pays
d’Europe et d’Amérique du Nord.

La lutte contre l’antisémitisme a été de plus en plus instrumentalisée
ces dernières années par le gouvernement israélien et ses partisans dans
un effort systématique visant à délégitimer la cause palestinienne et à
réduire au silence les défenseurs des droits des Palestiniens.

Détourner ainsi le combat nécessaire contre l’antisémitisme pour le
mettre au service d’une telle entreprise menace d’avilir ce combat et
partant de le discréditer et de l’affaiblir.

**L’antisémitisme doit être combattu et discrédité**.

La haine des Juifs en tant que Juifs ne saurait nulle part être tolérée,
quel qu’en soit le prétexte.

L’antisémitisme se manifeste à travers des stéréotypes et des généralisations
relatives aux Juifs, sur les thèmes de l’argent et du pouvoir en particulier,
ainsi que sous la forme de théories du complot et du négationnisme de
la Shoah.
Nous estimons légitime et nécessaire de combattre ces tendances.

Nous croyons également que les leçons à tirer de la Shoah, comme des
autres génocides des temps modernes, doivent être partie intégrante de
l’éducation des nouvelles générations contre toute forme d’hostilité
et de haine raciales.

Ce combat contre l’antisémitisme doit cependant être fondé sur des
principes, faute de quoi il contredirait sa propre finalité.
Par certains des « exemples » qu’elle fournit, la définition de l’AIMH
présuppose que tout Juif est sioniste et que l’État d’Israël dans sa
présente réalité incarne l’auto-détermination de tous les Juifs.

Nous sommes en profond désaccord avec ce postulat.

Le combat contre l’antisémitisme ne saurait être transformé en stratagème
pour délégitimer la lutte contre l’oppression des Palestiniens, contre
la négation de leurs droits et l’occupation continue de leur terre.

Nous considérons que les principes qui suivent sont fondamentaux
à cet égard.

1. Le combat contre l’antisémitisme doit se dérouler dans le cadre du
   droit international et des droits humains. Il doit être indissociable
   de la lutte contre toutes les formes de racisme et de xénophobie,
   y compris l’islamophobie et les racismes anti-arabe et anti-palestinien.
   Ce combat doit avoir pour finalité d’assurer la liberté et l’émancipation
   de tout groupe humain opprimé.
   Il est donc profondément dénaturé quand on le dévie pour servir à la
   défense d’un État oppresseur et prédateur.

2. La différence est considérable entre le cas de Juifs discriminés,
   opprimés et persécutés en tant que minorité par des régimes ou groupes
   antisémites et celui de l’autodétermination d’une population juive en
   Palestine accomplie sous la forme d’un État ségrégationniste sur le
   plan ethnique et expansionniste sur le plan territorial.
   Tel qu’il est actuellement, l’État d’Israël est fondé sur le
   déracinement de la grande majorité des autochtones – ce que Palestiniens
   et Arabes appellent la Nakba –, l’assujettissement de ceux qui résident
   encore dans le territoire de la Palestine historique, confinés dans
   un statut de citoyens de second ordre ou de peuple sous occupation,
   et leur privation de leur droit à l’autodétermination.

3. La définition de l’antisémitisme par l’AIMH, ainsi que les dispositions
   légales qui en ont découlé dans plusieurs pays, ont été principalement
   utilisées contre des groupes de gauche et des associations de défense
   des droits humains solidaires des droits des Palestiniens et contre
   la campagne Boycott, Désinvestissement, Sanctions (BDS), plutôt que
   contre la menace très réelle contre les Juifs que constituent les
   groupes d’extrême-droite nationalistes blancs en Europe et aux États-Unis.
   Qualifier la campagne BDS d’antisémite est une déformation grossière
   de ce qui est fondamentalement une forme de lutte non-violente et
   légitime pour les droits palestiniens.

4. La définition de l’AIMH fournit à titre d’exemple d’antisémitisme
   «le refus du droit à l’autodétermination des Juifs, en affirmant par
   exemple que l’existence de l’État d’Israël est le fruit d’une
   entreprise raciste ».
   C’est un bien étrange exemple qui ne s’encombre pas de prendre acte
   du fait qu’au regard du droit international, l’État d’Israël est une
   puissance occupante depuis plus d’un demi-siècle, comme cela est
   reconnu par les gouvernements des pays où la définition de l’AIMH
   est admise.
   Tout comme il ne s’encombre pas de juger si ce droit à l’autodétermination
   inclut celui de créer une majorité juive par voie de purification
   ethnique et s’il doit être considéré en prenant en compte les droits
   du peuple palestinien.
   En outre, la définition de l’AIMH est de nature à disqualifier en
   tant qu’antisémite toute vision non-sioniste de l’avenir de l’État
   israélien à l’instar des plaidoyers pour un État binational ou pour
   un État laïc et démocratique fondé sur l’égalité de tous ses citoyens
   et citoyennes.
   L’adhésion sincère au principe du droit des peuples à l’autodétermination
   ne saurait en exclure la nation palestinienne ou toute autre nation.

5. Nous croyons que le droit à l’autodétermination ne saurait inclure
   le droit de déraciner un autre peuple et de lui interdire de retourner
   à son territoire, ou tout autre moyen de s’assurer une majorité
   démographique au sein de l’État.
   La revendication palestinienne d’un droit au retour au pays dont
   eux-mêmes, leurs parents et grands-parents ont été expulsés ne
   saurait être qualifiée d’antisémite.
   Le fait qu’une telle demande suscite de l’angoisse chez des Israéliens
   ne prouve en rien qu’elle soit injuste ou antisémite.
   Il s’agit là d’un droit reconnu par le droit international, consacré
   en 1948 par la résolution 194 de l’assemblée générale des Nations unies.

6. Accuser d’antisémitisme quiconque considère que l’État d’Israël
   actuel est raciste, et cela en dépit des discriminations institutionnelles
   et constitutionnelles sur lesquelles se fonde cet État, équivaut à
   gratifier Israël d’une impunité absolue.
   Israël peut ainsi bannir ses citoyens palestiniens, les dépouiller
   de leur citoyenneté ou les priver du droit de vote, tout en demeurant
   immunisé contre toute accusation de racisme.
   La définition de l’AIMH et la façon dont elle a été utilisée interdisent
   toute critique de l’État d’Israël comme étant fondé sur une discrimination
   ethno-religieuse. Ceci est en contradiction avec les principes élémentaires
   de la justice, des droits humains et du droit international.

7. Nous croyons que la justice requiert un plein appui au droit à
   l’autodétermination des Palestiniens, y compris leur revendication
   de mettre fin à l’occupation de leur territoire reconnue internationalement
   en tant que telle, ainsi qu’à la spoliation infligée aux réfugiés
   palestiniens.
   L’oblitération des droits des Palestiniens dans la définition de
   l’antisémitisme par l’AIMH révèle une attitude qui défend la préséance
   des Juifs en Palestine plutôt que leurs droits, et leur suprématie
   sur les Palestiniens plutôt que leur sécurité.
   Nous croyons que les valeurs et les droits humains sont indivisibles
   et que la lutte contre l’antisémitisme doit aller de pair avec le
   soutien de la lutte de tous les peuples et groupes opprimés pour
   la dignité, l’égalité et l’émancipation.


