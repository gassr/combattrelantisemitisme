
Message de Corcuff Philippe
================================

Le jeudi 28 janvier 2021 à 23:39, Corcuff Philippe <philippe.corcuff@sciencespo-lyon.fr> a écrit :



============
Bonsoir,
============

Le RAAR (réseau national auquel je participe) est à l'initiative d'un
rassemblement contre les crimes antisémites et racistes à l'occasion
de la commémoration de l'assassinat d'Ilan Halimi le 14 février.


Le texte ci-après est destiné à des organisations : ça peut donc être la
FA, des groupes de la FA ou des structures syndicales ou associatives
auxquelles vous participez.

Les signatures sont à envoyées rapidement à raar@riseup.net .


salutations libertaires

Philippe, GGV


Commémoration pour Ilan Halimi : 15 ans déjà

Rassemblement contre les crimes antisémites et tous les actes racistes.


Dimanche 14 Février - 15h - 229 Bd Voltaire


Il y a 15 ans, Ilan Halimi était kidnappé, séquestré, torturé et assassiné
parce que Juif.

Il était retrouvé le 13 février agonisant sur les rails du RER C.
Ilan manque à sa famille et à ses proches. Et nous ne l'oublierons jamais.

Ce sont les stéréotypes antisémites les plus anciens qui ont mené à son
assassinat : les Juifs seraient riches et maniganceraient dans l'ombre.


Depuis 2006, de l'école Ozar Hatorah à Toulouse jusqu'à l'Hypercacher
de la Porte de Vincennes, onze hommes, femmes, enfants ou personnes âgées
ont été tuées en France parce que Juives.

Les actes antisémites et les violences racistes sont en augmentation,
ici et ailleurs dans le monde.

Les vingt-quatre jours qui ont amené à la mort tragique d'Ilan Halimi
nous obligent à une détermination sans faille contre l'antisémitisme
et contre toute forme de racisme.

C'est pourquoi le dimanche 14 février 2021 à 15h nous appelons, en sa mémoire,
ainsi qu'à celle de toutes les victimes de crimes antisémites et racistes,
à un rassemblement au 229 boulevard Voltaire, où Ilan travaillait.


Nous demandons explicitement qu'aucun drapeau national ne soit brandi,
afin de ne pas détourner le sens de cette commémoration.


A l'appel de :

Réseau d'Actions contre l'Antisémitisme et tous les Racismes, Juives et
Juifs révolutionnaires, Mémorial 98, Union des Juifs pour la Résistance
et l'Entraide

Centralisation des signataires : raar@riseup.net
