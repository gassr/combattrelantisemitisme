.. index::
   pair: Communiqué; 2021-01-21

.. _raar_communique_2021_01_21:

=====================================================================================================================
Premier communiqué du RAAR Réseau d'Actions contre l'Antisémitisme et tous les Racismes le jeudi 21 janvier 2021
=====================================================================================================================

.. contents::
   :depth: 3

Courriel
=========

::

    De : Réseau d'Actions contre l'Antisémitisme et tous les Racismes <raar@riseup.net>
    Envoyé : jeudi 21 janvier 2021 08:37
    À : RAAR Objet : Pour un Réseau d'Actions Contre l'Antisémitisme et tous les Racismes (RAAR)


Bonjour,

Nous vous informons de la constitution d'un **Réseau d'Actions Contre l'
Antisémitisme et tous les Racismes (RAAR)** en ce jour du 21 Janvier 2021.

En mémoire de l'enlèvement d'Ilan Halimi, séquestré, torturé et tué
parce que juif il y a aujourd'hui 15 ans.

Vous trouverez ci-après :

- Un :ref:`premier communiqué constitutif <premier_communique_raar>`;
- La :ref:`déclaration d'intention du réseau <declaration_intention_raar>`.

Nous nous tenons à votre disposition pour toute information complémentaire.

Bien cordialement,

Pour le RAAR

Facebook : https://www.facebook.com/R%C3%A9seau-dActions-contre-lAntis%C3%A9mitisme-et-tous-les-Racismes-110858790840212/


.. _premier_communique_raar:

Premier communiqué du RAAR (21 janvier 2021)
================================================


Pour un Réseau d'Actions contre l'Antisémitisme et tous les Racismes (RAAR)

Nous avons décidé de créer un réseau de lutte contre l'antisémitisme et
tous les racismes malgré les difficultés auxquelles nous faisons face
dans le contexte sanitaire et social actuel.

Nous choisissons la date de lancement du 21 janvier 2021 en mémoire de
l'enlèvement de Ilan Halimi, séquestré, torturé et tué parce que juif,
il y a quinze ans.
L'ignominie du crime avait défrayé la chronique, ainsi que l'antisémitisme
manifeste l'ayant motivé. Cette date représente la résurgence des meurtres
antisémites en France et fait de Ilan Halimi la première victime reconnue
comme assassinée par antisémitisme dans la période récente.

Le Réseau d'Actions contre l'Antisémitisme et tous les Racismes (RAAR)
regroupe des organisations, collectifs et individus partie prenante du
mouvement social.
Juives, juifs ou non, militant.es syndicales.aux, associatives.fs, ou
politiques encarté.e.s ou non, étudiant.es, universitaires, salarié.es,
sans-emploi, retraité.es, féministes, écologistes, toutes et tous
résolument anti-racistes.

Provenant de diverses régions, nous nous sommes réunis la première fois
en septembre 2019, dressant un constat commun : la difficulté d'identifier
et de nommer l'antisémitisme actuel y compris lorsque ce déni émane de
notre propre camp politique.

Notre objectif : donner des clefs pour la renaissance d'un mouvement
progressiste uni qui affiche ses valeurs et lutte contre la haine et
toutes les discriminations.

Ce réseau nous paraît plus que jamais nécessaire face aux thèses complotistes
et antisémites auxquelles la pandémie a donné une vigueur nouvelle, afin
d'apporter une lecture claire des mécanismes et propagandes haineux et
contribuer à une prise de conscience.

Cette démarche pédagogique servira notamment à combattre le terrorisme
djihadiste et les courants islamistes pour qui l'antisémitisme est central,
autant que les attaques racistes de ceux qui n'ont de cesse de s'acharner
sur les musulman-es, les rendant en plus responsables du "nouvel antisémitisme",
comme si l'antisémitisme historique cher à la "vieille France" avait disparu.

Le complotisme autour de la pandémie vient s'ajouter à un antisémitisme
qui n'a eu de cesse de se développer ces dernières années

La pandémie de Covid-19 donne lieu à un déluge de messages complotistes
à **tonalité antisémite**.

Dès mars 2020, médecins, laboratoires pharmaceutiques et personnalités
juives sont accusés de comploter contre la santé de la population mondiale.
Ces accusations, réminiscences des pires traditions antisémites attribuant
aux Juives/Juifs les épidémies du passé par empoisonnement des puits, sont
utilisées par différents camps politiques afin de gagner en popularité.

Les graves événements du 6 janvier 2021 à Washington ne laissent aucune
place à l’ambiguïté.
Des centaines de partisan.e.s de Donald Trump encouragé.e.s par ce dernier,
envahissent le Capitole pour empêcher l'investiture de Joe Biden.
Parmi ces activistes ultra-conservateurs dont beaucoup sont armé-es, des
suprémacistes sudistes pro-esclavagisme et des néo-nazis sont identifi-ées.
La menace raciste et antisémite est concrète et s'affiche au grand jour.
Encore une fois, les réactions politiques ont été bien faibles sur ce point.

Ces événements ont lieu alors même que de nombreux actes antisémites se
multiplient en Europe depuis des années : profanations de sépultures,
agressions, insultes sur les réseaux sociaux...
On se souvient de l'attentat d'extrême droite à la synagogue de Halle en
2019 le jour de la fête de Kippour, dont l'auteur vient d'être condamné
ou de l'attentat djihadiste de l'Hyper Cacher.

Plus récemment, c'est une dauphine de Miss France, **April Benayoum**, qui a
massivement été insultée et menacée en référence à la Shoah avec des
messages proclamant "Tonton Hitler, t'as oublié d'exterminer Miss Provence",
en raison des origines israéliennes de son père.
Si descondamnations ont bien été entendues, des réactions ambivalentes,
voire justificatrices ont également été exprimées.

Un réseau d'actions
=====================

Face à l'augmentation constante des faits et violences antisémites et
racistes en France, en Europe et aux Etats-Unis, le RAAR appelle à la
mobilisation contre toutes les formes de propagande haineuse et de violence.

Le RAAR s'adresse au plus grand nombre afin de susciter et accompagner
une large prise de conscience et d'organiser les ripostes qui s'imposent.

Il adresse en particulier un message de solidarité fraternelle à toutes
celles et ceux qui aux Etats-Unis font face à la violence d'extrême
droite qui prend pour cible les minorités, dont les Noir.e.s et les Juives/Juifs.

21 Janvier 2021
Contact : raar@riseup.net


.. _declaration_intention_raar:

Déclaration d'intention du RAAR
==================================

Pour un **Réseau d’Actions contre l’Antisémitisme et tous les Racismes (RAAR)**

La création de ce réseau s'appuie sur plusieurs constats partagés:

- La recrudescence de l’antisémitisme sous toutes ses formes, dans un
  contexte de montée des racismes et d’aggravation de la crise sociale.
  Antisémitisme qui reprend les thèmes traditionnels sur le prétendu
  pouvoir des Juifs et le fantasmatique contrôle des médias ; leur situation
  de privilégiés, voire leur richesse (comme les allusions à la banque
  Rothschild), la protection dont ils bénéficieraient ; l’affirmation
  qu’ils seraient des agents d’influence de l’étranger.
- Nous constatons que l’antisémitisme a tué plusieurs fois au cours des
  16 dernières années : **Sébastien Selam** en 2003, **Ilan Halimi** en 2006 à Bagneux ;
  **Jonathan, Arié et Gabriel Sandler**, et **Myriam Monsonego** en 2012
  à Toulouse à l’école Ozar Hatorah ; **Yohan Cohen, Philippe Braham**,
  **François-Michel Saada et Yoav Hattab** en 2015 à l’Hypercacher ;
  **Sarah Halimi** en 2017 et **Mireille Knoll** en 2018, toutes deux à Paris.
  A ces meurtres s’ajoutent des viols à caractère antisémite comme
  celui de L. à Créteil en 2014.
- L’antisémitisme se traduit au quotidien par des paroles ou comportements
  à caractère discriminatoire, des harcèlements et agressions à l’école,
  à l’université, au travail, dans la rue ou sur les réseaux sociaux.
- **Nous notons que beaucoup trop de partisans de l’émancipation sociale
  et des combats quotidiens contre l’exploitation et les discriminations
  reproduisent des mécanismes qui les conduisent à minimiser ou à taire
  les actes et énoncés antisémites**.
  Ils ne se soucient pas de cette situation alarmante et vont jusqu’à nier
  cette recrudescence de l’antisémitisme.
  Ils se sont absentés des mobilisations lors de l’assassinat d’Ilan
  Halimi en 2006, des meurtres de l’école Ozar Hatorah à Toulouse en 2012,
  et de l’Hyper Cacher en 2015, abandonnant ainsi le terrain aux forces
  réactionnaires. Cela a contribué à un brouillage délétère.
  Ces mécanismes doivent être analysés pour mieux être détruits.
- Nous combattons l’antisémitisme d’où qu’il vienne, quels que soient ses
  prétextes, ses motivations et ses justifications.

Notre réseau entend lutter contre l’antisémitisme:

- En rappelant que la droite et l’extrême droite, fidèles à leur xénophobie,
  veulent faire croire que l’antisémitisme viendrait d’**ailleurs** et
  ne plongerait pas ses racines dans plusieurs traditions politiques
  françaises vieilles de plusieurs siècles.
- En rappelant qu’un antisémitisme diffus est resté bien vivace et qu’il
  est nourri aujourd’hui par tous les intégrismes religieux.
- En rejetant toute interprétation de l’antisémitisme comme provenant
  essentiellement de la population musulmane.
  Cette position est instrumentalisée par la droite et l’extrême droite
  et reprise dans certains secteurs de la gauche.
- **En répertoriant et analysant les formes de banalisation de l’antisémitisme
  du côté de la gauche et l’extrême gauche, hier comme aujourd’hui**.
  Cette banalisation plonge dans une histoire du mouvement ouvrier qui
  n’en a jamais été exempte.
  En critiquant les positionnements qui, à gauche, refusent de voir la
  réalité de l’antisémitisme et le cantonnent exclusivement à l’extrême droite.
- En dénonçant toute complaisance envers l’antisémitisme, que ce soit au
  prétexte de l’antisionisme ou de la *lutte contre la finance*.
  La lutte contre l’antisémitisme doit faire partie intégrante de l’action
  contre toutes les formes de racisme.

  **Et nous refusons l’injonction selon laquelle il faudrait d’abord
  adopter une position sur Israël avant de pouvoir dénoncer la haine
  des Juifs.**
- En participant aux luttes contre tous les autres racismes et en apportant
  notre solidarité à toutes leurs victimes.
  Nous refusons notamment l’opposition entre les actions contre l’antisémitisme
  et celles contre l’islamophobie, le racisme antimusulman.
- En nous solidarisant avec les étrangers et les étrangères en butte à la répression de l’État.

Nos actions
============

- Mettre à la disposition du plus grand nombre des outils (argumentaires,
  objets de diffusion physiques et numériques) pour agir contre l’antisémitisme
  vécu au quotidien.
  Y compris dans les luttes et sur les lieux de travail.
- Organiser des réunions publiques ou des manifestations, seuls ou avec
  des mouvements et personnes qui partagent nos préoccupations.
- Réagir contre des actes, manifestations et publications antisémites
  et/ou négationnistes.
- Participer aux manifestations contre l’antisémitisme en y faisant
  apparaître nos positions.
- Continuer à débattre des aspects historiques et politiques de l’antisémitisme,
  proposer des espaces de formation et analyser de manière plus approfondie
  les mécanismes de l’antisémitisme contemporain.

