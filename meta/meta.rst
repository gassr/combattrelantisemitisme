.. index::
   ! meta-infos


.. _antisem_meta_infos:

=====================
Meta infos
=====================

.. seealso::

   - https://cnt-f.gitlab.io/meta/


.. contents::
   :depth: 3


Gitlab project
================

.. seealso::

   - https://framagit.org/france1/combattrelantisemitisme


Issues
--------

.. seealso::

   - https://framagit.org/france1/combattrelantisemitisme/-/boards


Sphinx theme : **sphinx_book_theme**
============================================

.. seealso::

   - https://gdevops.gitlab.io/tuto_documentation/doc_generators/sphinx/themes/sphinx_book_theme/sphinx_book_theme.html


::

    liste_full = [
        "globaltoc.html",
    html_theme = "sphinx_book_theme"
    copyright = f"2011-{now.year}, assr38, Creative Commons CC BY-NC-SA 3.0. Built with sphinx {sphinx.__version__} Python {platform.python_version()} {html_theme=}"




pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
